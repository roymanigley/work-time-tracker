# Work Time Tracker
> - Displays workingtimes for a specific day
> - Traks time used while working
> - Workingtimes can be tagged anf filtered

## Installation
### Requirements
- JDK 8
- Apache Maven
- npm 6.13.4
- node 10.18.0

### Bulding the Application

1. Build the client  
> default REST URL for non productive builds is `/` defined in `src/env/environment.prod.ts`
`ng build --prod`
2. Copy the `dist` content to the servers `static` resources  
`rm -rf ../server/src/main/resources/static/* && cp dist/work-time-tracker/* ../server/src/main/resources/static -r`
3. Build the server  
`mvn clean install`  


### Run the Application
#### As jar
`java -Dserver.port=8080 -jar target/work-time-tracker-1.0.jar`
#### Client only
> default REST URL for non productive builds is http://localhost:8080/ defined in `src/env/environment.ts`
`ng serve --host 0.0.0.0`
#### Server only
`mvn clean spring-boot:run`