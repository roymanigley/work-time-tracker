import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material';
import { WorkTime } from 'src/app/model/work-time';
import { WorkTimeService } from 'src/app/service/work-time.service';
import { WorkTimeListComponent } from '../work-time-list/work-time-list.component';
import { SnackBarUtil } from 'src/app/utils/SnackBarUtil';
import { WorktimeDeleteDialog } from './delete-dialog/work-time-delete-dialog';

@Component({
  selector: 'app-work-time-edit',
  templateUrl: './work-time-edit.component.html',
  styleUrls: ['./work-time-edit.component.css']
})
export class WorkTimeEditComponent implements OnInit {

  selectedWorkTime : WorkTime;

  timeWorkedInHoursOnly = 0;
  timeWorkedInMinutesOnly = 0;
  timeWorkedInSecondsOnly = 0;

  showEditWorkingTime = false;
  showWorkingTimeTable = false;

  constructor(
    private workTimeService: WorkTimeService
    , private workTimeListComponent: WorkTimeListComponent
    , private snackBarUtil: SnackBarUtil
    , private dialog: MatDialog) { }

  ngOnInit() {
    this.initInterval();
  }

  createWorkTime(): void {
    this.showWorkingTimeTable = false;
    this.showEditWorkingTime = false;
    this.selectedWorkTime = new WorkTime();
  }

  saveWorkTime(): void {
    this.saveWorkTimeAndKeepView();
  }

  saveWithChangedWorkingTime(): void {
    this.selectedWorkTime.setTimeWorkedInClockFormat(
      this.timeWorkedInHoursOnly,
      this.timeWorkedInMinutesOnly,
      this.timeWorkedInSecondsOnly
    );
    this.saveWorkTimeAndKeepView();
    this.hideChangedWorkingTime();
  }

  saveWorkTimeAndKeepView(): void {
    this.workTimeService.save(this.selectedWorkTime).subscribe(data => {
      this.snackBarUtil.showSuccessMessage("WorkTime: " + this.selectedWorkTime.title, "SAVED")
      this.selectedWorkTime = Object.assign(new WorkTime(), data);
      this.workTimeListComponent.initWorkTimes();
    }, error => {
      console.error(error.message);
      this.snackBarUtil.showErrorMessage("Could not save WorkTime: "  + error.statusText + " (" + error.status + ")", "ERROR")
    });
  }

  deleteWorkTime(): void {
    const dialogRef = this.dialog.open(WorktimeDeleteDialog, {
      width: '250px',
      data: this.selectedWorkTime
    });

    dialogRef.afterClosed().subscribe(result => {
      this.workTimeListComponent.initWorkTimes();
      this.cancelWorkTime();
    });
  }
  
  cancelWorkTime(): void {
    this.selectedWorkTime = null;
    this.workTimeListComponent.initWorkTimes();
    this.hideChangedWorkingTime();
  }

  selectWorkTime(workTime: WorkTime): void {
    this.showWorkingTimeTable = false;
    this.showEditWorkingTime = false;
    this.selectedWorkTime = workTime;
  }  

  showHideWorkingTimeTable(): void {
    this.showWorkingTimeTable = !this.showWorkingTimeTable;
    this.workTimeListComponent.initWorkTimes();
  }

  showEditWorkingTimeControls(): void {
    this.showEditWorkingTime = true;
    this.timeWorkedInHoursOnly = this.selectedWorkTime.timeWorkedInHoursOnly();
    this.timeWorkedInMinutesOnly = this.selectedWorkTime.timeWorkedInMinutesOnly();
    this.timeWorkedInSecondsOnly = this.selectedWorkTime.timeWorkedInSecondsOnly();
  }

  hideChangedWorkingTime(): void {
    this.showEditWorkingTime = false;
    this.timeWorkedInHoursOnly = 0;
    this.timeWorkedInMinutesOnly = 0;
    this.timeWorkedInSecondsOnly = 0;
  }

  private initInterval(): void {
    
    setInterval((handler) => {
      if (this.selectedWorkTime != null) {
        this.selectedWorkTime.timeWorkedSeconds++;
      }
    }, 1000);
  }

}
