import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { FormControl } from '@angular/forms';
import { ENTER, COMMA } from '@angular/cdk/keycodes';
import { Observable } from 'rxjs';
import { Tag } from 'src/app/model/tag';
import { MatAutocomplete, MatAutocompleteSelectedEvent, MatChipInputEvent } from '@angular/material';
import { startWith, map } from 'rxjs/operators';
import { TagService } from 'src/app/service/tag.service';
import { SnackBarUtil } from 'src/app/utils/SnackBarUtil';
import { WorkTimeEditComponent } from '../work-time-edit.component';

@Component({
  selector: 'app-tag-input',
  templateUrl: './tag-input.component.html',
  styleUrls: ['./tag-input.component.css']
})
export class TagInputComponent implements OnInit {

  // Tags Properties
  visible = true;
  selectable = true;
  removable = true;
  addOnBlur = true;
  separatorKeysCodes: number[] = [ENTER, COMMA];
  tagCtrl = new FormControl();
  filteredTags: Observable<Tag[]>;
  allTags: Tag[] = [];
  

  @ViewChild('tagInput', {static: false}) tagInput: ElementRef<HTMLInputElement>;
  @ViewChild('auto', {static: false}) matAutocomplete: MatAutocomplete;
  
  constructor(
    private tagService: TagService
    , private snackBarUtil: SnackBarUtil
    , public workTimeEditComponent: WorkTimeEditComponent) { 
      
    this.filteredTags = this.tagCtrl.valueChanges.pipe(
      startWith(null),
      map((tag: string | null) => tag ? this._filterTags(tag) : this.allTags.slice()));
  }

  ngOnInit() {
    this.initTags();
  }

  private initTags(): void {
    console.error("INIT TAG");
    this.tagService.findAll(this.workTimeEditComponent.selectedWorkTime).subscribe(data => this.allTags = data, error => {
      console.error(error.message);
      this.snackBarUtil.showErrorMessage("Could not fetch Tags: "  + error.statusText + " (" + error.status + ")", "ERROR")
    });
  }


  private _filterTags(value: any): Tag[] {
    let filterValue = "";
    if (value.label) {
      filterValue = value.label.toLowerCase();
    } else {
      filterValue = value.toLowerCase();
    }
    return this.allTags.filter(tag => tag.label.toLowerCase().indexOf(filterValue) > -1);  
  }


  addTag(event: MatChipInputEvent): void {
    // Add tag only when MatAutocomplete is not open
    // To make sure this does not conflict with OptionSelected Event
    if (!this.matAutocomplete.isOpen) {
      const input = event.input;
      const value = event.value;

      // Add our tag
      if ((value || '').trim()) {
        let tag = new Tag(value.trim());
        this.tagService.addTag(this.workTimeEditComponent.selectedWorkTime, tag).subscribe((tag) => {
          this.snackBarUtil.showSuccessMessage("Tag: " + tag.label, "SAVED")
          this.workTimeEditComponent.selectedWorkTime.tags.push(tag);
          this.initTags();
        }, error => {
          console.error(error.message);
          this.snackBarUtil.showErrorMessage("Could not save Tag: "  + error.statusText + " (" + error.status + ")", "ERROR")
        })
      }

      // Reset the input value
      if (input) {
        input.value = '';
      }

      this.tagCtrl.setValue(null);
    }
  }

  removeTag(tag: Tag): void {
    const index = this.workTimeEditComponent.selectedWorkTime.tags.indexOf(tag);

    if (index >= 0) {
      this.workTimeEditComponent.selectedWorkTime.tags.splice(index, 1);
    }
    
    if (tag.id) 
      this.tagService.deleteTag(this.workTimeEditComponent.selectedWorkTime, tag).subscribe(
        () => this.snackBarUtil.showSuccessMessage("Tag: " + tag.label, "DELETED"), error => {
        console.error(error.message);
        this.snackBarUtil.showErrorMessage("Could not delete Tag: "  + error.statusText + " (" + error.status + ")", "ERROR")
      })
    
  }

  selectTag(event: MatAutocompleteSelectedEvent): void {
    let tag = new Tag(event.option.viewValue)
    this.tagService.addTag(this.workTimeEditComponent.selectedWorkTime, tag).subscribe((tag) => {
      this.workTimeEditComponent.selectedWorkTime.tags.push(tag);
      this.snackBarUtil.showSuccessMessage("Tag: " + tag.label, "SAVED")
    }, error => {
      console.error(error.message);
      this.snackBarUtil.showErrorMessage("Could not save Tag: "  + error.statusText + " (" + error.status + ")", "ERROR")
    })
    this.tagInput.nativeElement.value = '';
    this.tagCtrl.setValue(null);
  }
}
