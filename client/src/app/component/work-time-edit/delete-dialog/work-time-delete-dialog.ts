import { Component, Inject } from '@angular/core';
import { WorkTime } from 'src/app/model/work-time';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { WorkTimeService } from 'src/app/service/work-time.service';
import { SnackBarUtil } from 'src/app/utils/SnackBarUtil';

@Component({
    selector: 'work-time-delete-dialog',
    templateUrl: 'work-time-delete-dialog.html',
  })
  export class WorktimeDeleteDialog {
  
    constructor(
      public dialogRef: MatDialogRef<WorktimeDeleteDialog>
      , @Inject(MAT_DIALOG_DATA) public data: WorkTime 
      , public workTimeService: WorkTimeService
      , public snackBarUtil: SnackBarUtil) {}
  
    onNoClick(): void {
      this.dialogRef.close();
    }

    onYesClick(): void {
      let title = this.data.title;
      this.workTimeService.delete(this.data).subscribe(() => {
        this.snackBarUtil.showSuccessMessage("WorkTime: " + title, "DELETED")
        this.dialogRef.close();
      }, error => {
        console.error(error.message);
        this.snackBarUtil.showErrorMessage("Could not delete WorkTime: "  + error.statusText + " (" + error.status + ")", "ERROR")
      });
    }
  
  }