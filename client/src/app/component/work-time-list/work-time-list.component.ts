import { Component, OnInit, Inject, ViewChild } from '@angular/core';
import { WorkTimeService } from '../../service/work-time.service';
import { WorkTime } from 'src/app/model/work-time';
import { MatTableDataSource } from '@angular/material';
import { WorkTimeEditComponent } from '../work-time-edit/work-time-edit.component';
import { SnackBarUtil } from 'src/app/utils/SnackBarUtil';
import { FormControl } from '@angular/forms';

@Component({
  selector: 'app-work-time-list',
  templateUrl: './work-time-list.component.html',
  styleUrls: ['./work-time-list.component.css'],
  providers: []
})
export class WorkTimeListComponent implements OnInit {

  selectedDateFrom = new Date();
  selectedDateTo = new Date();
  totalTime: string;
  displayedColumns = [
    {key: 'date', visible: navigator.userAgent.toLowerCase().indexOf('mobile') == -1}, 
    {key: 'title', visible: true}, 
    {key: 'description', visible: navigator.userAgent.toLowerCase().indexOf('mobile') == -1}, 
    {key: 'tags', visible: navigator.userAgent.toLowerCase().indexOf('mobile') == -1}, 
    {key: 'timeWorked', visible: true}, 
    {key: 'buttons', visible: true}
  ];
  dataSource: MatTableDataSource<WorkTime>;

  @ViewChild("editWorkTimeComponent", {static: false}) worktimeEdit: WorkTimeEditComponent;

  constructor(
    private workTimeService: WorkTimeService
    , private snackBarUtil: SnackBarUtil) { }

  ngOnInit() {
    this.initWorkTimes();    
  }

  initWorkTimes(): void {
   
    this.workTimeService.findAll(this.selectedDateFrom, this.selectedDateTo)
      .subscribe(data => {
        this.dataSource = new MatTableDataSource(
          data.map(workTime => Object.assign(new WorkTime(), workTime))
        );
        
        this.dataSource.filterPredicate = (data: WorkTime, filterValue: string) => {
          if (filterValue.startsWith("#"))
            return data.tags.filter(tag => ("#" + tag.label).toLocaleLowerCase().indexOf(filterValue) >= 0).length > 0
          else if (filterValue.startsWith("!#"))
            return data.tags.filter(tag => ("!#" + tag.label).toLocaleLowerCase().indexOf(filterValue) >= 0).length <= 0
          return (data.title + " " + data.description).toLocaleLowerCase().indexOf(filterValue) >= 0;
        };
      this.updateTotalTime();
    }, error => {
      console.error(error.message);
      this.snackBarUtil.showErrorMessage("Could not fetch WorkTimes: "  + error.statusText + " (" + error.status + ")", "ERROR")
    });
  }

  createWorkTime(): void {
    this.worktimeEdit.createWorkTime();
  }

  selectWorkTime(workTime: WorkTime): void {
    this.worktimeEdit.selectWorkTime(workTime);
  }

  applyFilter(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLowerCase();
    this.updateTotalTime();
  }

  private updateTotalTime(): void {
    let workTimeSum = new WorkTime();
    this.dataSource.filteredData.forEach(data => {
      workTimeSum.timeWorkedSeconds+= data.timeWorkedSeconds
    });
    this.totalTime = workTimeSum.timeWorkedInClockFormat();
  }

  getDisplayedColumns(): string[] {

    let val = this.displayedColumns
      .filter( col => col.visible)
      .map(col => col.key)
      return val
  }
}
