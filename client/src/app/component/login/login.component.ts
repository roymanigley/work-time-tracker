import { Component, OnInit } from '@angular/core';
import { AuthService } from 'src/app/service/auth.service';
import { SnackBarUtil } from 'src/app/utils/SnackBarUtil';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  username: string;
  password: string;

  constructor(private authService: AuthService, private snackBarUtil: SnackBarUtil) { }

  ngOnInit() {
  }

  login(): void {

    this.authService.login(this.username, this.password, 
      () => { this.snackBarUtil.showSuccessMessage("Hello " + this.username, "LOGIN") }, 
      () => { this.snackBarUtil.showErrorMessage("Login failed for " + this.username, "LOGIN") });
  }
}
