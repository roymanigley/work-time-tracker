import { Injectable } from '@angular/core';
import { MatSnackBar } from '@angular/material';

@Injectable({
  providedIn: 'root'
})
export class SnackBarUtil {

  constructor(private _snackBar: MatSnackBar) { }

  public showInfoMessage(message: string, action: string): void {
    this.showMessageWithDuration(message, action, 2000, "snackbar-info");
  }

  public showSuccessMessage(message: string, action: string): void {
    this.showMessageWithDuration(message, action, 2000, "snackbar-success");
  }

  public showErrorMessage(message: string, action: string): void {
    this.showMessageWithDuration(message, action, -1, "snackbar-error");
  }
  
  private showMessageWithDuration(message: string, action: string, duration: number, styleClass: string): void {
    this._snackBar.open(message, action, {
      duration: duration,
      panelClass: styleClass
    });
  } 
}
