export class Tag {

    id: Number
    label: string;

    constructor(label: string) {
        this.label = label;
    }
}
