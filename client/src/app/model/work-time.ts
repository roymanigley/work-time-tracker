import { Tag } from './tag';

export class WorkTime {

    id: Number
    date: Date = new Date();
    title: string;
    description: string;
    timeWorkedSeconds: number = 0;
    tags: Tag[];
    constructor() {}
    
    timeWorkedInHoursOnly() {
        return Math.floor(this.timeWorkedSeconds / 60 / 60);
    }

    timeWorkedInMinutesOnly() {
        return Math.floor((this.timeWorkedSeconds - (this.timeWorkedInHoursOnly() * 60 * 60)) / 60);
    }

    timeWorkedInSecondsOnly() {
        return Math.floor(this.timeWorkedSeconds - ((this.timeWorkedInHoursOnly() * 60 * 60) + (this.timeWorkedInMinutesOnly() * 60)));
    }

    setTimeWorkedInClockFormat(hours: number, minutes: number, seconds: number) {
        this.timeWorkedSeconds = hours * 60 * 60 + minutes * 60 + seconds; 
    }

    public timeWorkedInClockFormat() : string {

        let hours = "0" + (this.timeWorkedInHoursOnly() || 0)
        let minutes = "0" + (this.timeWorkedInMinutesOnly() || 0)
        let seconds = "0" + (this.timeWorkedInSecondsOnly() || 0);
        
        return (hours).substring(hours.length - 2)
        + " : " + (minutes).substring(minutes.length - 2)
        + " : " + (seconds).substring(seconds.length - 2)
    }
}
