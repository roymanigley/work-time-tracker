import { Injectable } from '@angular/core';
import { WorkTime } from '../model/work-time';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class WorkTimeService {

  baseUrl = environment.restBaseUrl + 'api/WorkTime/';

  constructor(private httpClient: HttpClient) { }

  public findAll(dateFrom: Date, dateTo: Date): Observable<WorkTime[]> {
    let isoDateFrom = this.toUtcIsoString(dateFrom);
    let isoDateTo = this.toUtcIsoString(dateTo);
    return this.httpClient.get<WorkTime[]>(this.baseUrl + isoDateFrom + "/" + isoDateTo);
  } 

  private toUtcIsoString(date: Date): string {
    let monthStr = (date.getMonth()+1) + "";
    let dayStr = (date.getDate()) + "";
    return date.getFullYear() + "-" + ("0" + monthStr).substr(monthStr.length-1, 2) + "-" + ("0" + dayStr).substr(dayStr.length-1, 2);
  }

  public save(workTime :WorkTime): Observable<WorkTime> {
    if (workTime.id)
      return this.httpClient.put<WorkTime>(this.baseUrl + workTime.id, workTime);
    else
      return this.httpClient.post<WorkTime>(this.baseUrl, workTime);
  } 

  public delete(workTime :WorkTime): Observable<WorkTime> {
    return this.httpClient.delete<WorkTime>(this.baseUrl + workTime.id);
  }   
}
