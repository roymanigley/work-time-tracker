import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { environment } from 'src/environments/environment';
//import { UserService } from './user-service.service';
//import { User } from '../model/user';

@Injectable()
export class AuthService {
  
    private TOKEN_KEY = "JSESSIONID";
    private USERNAME_KEY = "USERNAME";
    private AUTH_BASIC_KEY = "AUTH_BASIC";
    private authApplicationLoginUrl = environment.restBaseUrl + "api/User/";
  
    constructor(private http: HttpClient) {
        
    }

    public login(username: String, password: String, onSuccess: Function, onError: Function): void {
                
        let headers = new HttpHeaders();
        let authBasic = btoa(username + ":" + password);
        headers = headers.append("Authorization", "Basic " + authBasic);
        
        sessionStorage.setItem(this.AUTH_BASIC_KEY, authBasic);

        this.http.get<Map<String, String>>(this.authApplicationLoginUrl, { headers }).subscribe((map: Map<String, String>) => {
            sessionStorage.setItem(this.TOKEN_KEY, map["TOKEN"]);
            sessionStorage.setItem(this.USERNAME_KEY, map["USERNAME"]);
        }, () => {
            onError.call(this);
        });
    }

    logout() {
        sessionStorage.removeItem(this.AUTH_BASIC_KEY);
        sessionStorage.removeItem(this.TOKEN_KEY);
        sessionStorage.removeItem(this.USERNAME_KEY);
    }
  
    isUserLoggedIn(): boolean {
        return sessionStorage.getItem(this.AUTH_BASIC_KEY) && sessionStorage.getItem(this.USERNAME_KEY) && sessionStorage.getItem(this.TOKEN_KEY) != null;
    }
    
    public getJSessionId(): String {        
        return sessionStorage.getItem(this.TOKEN_KEY);
    }
    
    public getAuthBasic(): String {        
        return sessionStorage.getItem(this.AUTH_BASIC_KEY);
    }
}