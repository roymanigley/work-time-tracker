import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { WorkTime } from '../model/work-time';
import { Tag } from '../model/tag';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class TagService {

  constructor(private httpClient: HttpClient) { }
     
  baseUrlWorkTimeTag = environment.restBaseUrl + "api/WorkTime/";
  baseUrlTags = environment.restBaseUrl + "api/Tag/";


  public findAll(workTime: WorkTime): Observable<Tag[]> {
    return this.httpClient.get<Tag[]>(this.baseUrlTags);
  } 

  public addTag(workTime: WorkTime, tag: Tag): Observable<Tag> {
    if (workTime.id)
      return this.httpClient.post<Tag>(this.baseUrlWorkTimeTag + workTime.id + "/Tag", tag);
  }

  public deleteTag(workTime: WorkTime, tag: Tag): Observable<Tag> {
    if (workTime.id && tag.id)
      return this.httpClient.delete<Tag>(this.baseUrlWorkTimeTag + workTime.id + "/Tag/" + tag.id);
  }
}
