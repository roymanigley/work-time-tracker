import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { MatToolbarModule, MatFormFieldModule, MatTableModule, MatInputModule, MatChipsModule, MatOptionModule, MatAutocompleteModule, MatDialogModule, MatDatepickerModule, MatNativeDateModule, MAT_DATE_FORMATS, MatSnackBarModule, MAT_DATE_LOCALE, MatProgressBarModule, MatMenuModule } from '@angular/material';
import {MatButtonModule} from '@angular/material/button';
import {MatIconModule} from '@angular/material/icon';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { WorkTimeListComponent } from './component/work-time-list/work-time-list.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { WorkTimeEditComponent } from './component/work-time-edit/work-time-edit.component';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { WorktimeDeleteDialog } from './component/work-time-edit/delete-dialog/work-time-delete-dialog';
import { LoaderComponent } from './component/loader/loader.component';
import { LoaderService } from './service/loader.service';
import { LoaderInterceptor } from './interceptor/loader.interceptor';
import { TagInputComponent } from './component/work-time-edit/tag-input/tag-input.component';
import { JsessionInterceptor } from './interceptor/jsession.interceptor';
import { AuthService } from './service/auth.service';
import { LoginComponent } from './component/login/login.component';

@NgModule({
  declarations: [
    AppComponent,
    WorkTimeListComponent,
    WorkTimeEditComponent,
    WorktimeDeleteDialog,
    LoaderComponent,
    TagInputComponent,
    LoginComponent
  ],
  entryComponents: [WorktimeDeleteDialog],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    MatToolbarModule,
    FormsModule,
    MatFormFieldModule,
    MatTableModule,
    MatInputModule,
    MatButtonModule,
    MatIconModule,
    MatChipsModule,
    MatAutocompleteModule,
    ReactiveFormsModule,
    MatDatepickerModule,
    MatNativeDateModule,
    MatSnackBarModule,
    HttpClientModule,
    MatDialogModule,
    MatProgressBarModule,
    MatMenuModule
  ],
  providers: [
    LoaderService,
    AuthService,
    { provide: HTTP_INTERCEPTORS, useClass: LoaderInterceptor, multi: true },
    { provide: HTTP_INTERCEPTORS, useClass: JsessionInterceptor, multi: true },
    {provide: MAT_DATE_LOCALE, useValue: 'de-CH'},
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
