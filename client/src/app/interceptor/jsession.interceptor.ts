import { HttpInterceptor, HttpRequest, HttpHandler, HttpEvent } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Injectable } from '@angular/core';
import { AuthService } from '../service/auth.service';

@Injectable()
export class JsessionInterceptor implements HttpInterceptor {
  constructor(public auth: AuthService) {}
  intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    //console.log("Intercepring Request: " + this.auth.getAuthBasic());
    //if (this.auth.getJSessionId() != null) {
        // Clone the request to add the new header

        //const clonedRequest = request.clone({ headers: request.headers.set('Set-Cookie', 'jsessionid=' + this.auth.getJSessionId()) });
        let headers = request.headers.set('Authorization', 'Basic ' + this.auth.getAuthBasic())
        //headers.append('Set-Cookie', 'jsessionid=' + this.auth.getJSessionId())
        
        const clonedRequest = request.clone({ headers: headers });
        // Pass control to the next request
        return next.handle(clonedRequest);
        //return next.handle(request);
        //} else {
        // Pass control to the next request
     //   return next.handle(request);
    //}
  }
}