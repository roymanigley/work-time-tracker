# Work Time Tracker

## To Do

- Authentication  
  [JWT Authentication Example](https://dzone.com/articles/angular-7-spring-boot-jwt-authentication-example)

## Installation

### Dependencies
- Apache Maven
- JDK 8

### Build
`mvn clean install`

### Run
#### with maven
`mvn clean spring-boot:run`
#### as .jar
`java -Dserver.port=8080 -jar target/work-time-tracker-1.0.jar`