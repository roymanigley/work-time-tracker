package ch.bytecrowd.worktimetracker;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class WorkTimeTrackerApplication {

	public static void main(String[] args) {
		SpringApplication.run(WorkTimeTrackerApplication.class, args);
	}

}
