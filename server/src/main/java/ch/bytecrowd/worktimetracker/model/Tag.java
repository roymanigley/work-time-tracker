package ch.bytecrowd.worktimetracker.model;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;

@Entity
@Table(name = "BC_TAG")
public class Tag implements Comparable<Tag> {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "ID")
    private Long id;

    @Column(name = "LABEL", nullable = false)
    private String label;

    @ManyToMany
    @JsonIgnore
    private Set<WorkTime> workTimes = new HashSet<>();

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public Set<WorkTime> getWorkTimes() {
        return workTimes;
    }


    public void addWorkTime(WorkTime workTime) {
        this.workTimes.add(workTime);
    }

    public void removeWorkTime(WorkTime workTime) {
        workTime.getTags().remove(this);
        this.workTimes.remove(workTime);
    }

    @Override
    public int compareTo(Tag tag) {
        return this.label.compareTo(tag.label);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Tag tag = (Tag) o;
        return Objects.equals(id, tag.id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id);
    }
}