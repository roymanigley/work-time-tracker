package ch.bytecrowd.worktimetracker.model;

import javax.persistence.*;
import java.util.Date;
import java.util.Objects;
import java.util.Set;
import java.util.TreeSet;

@Entity
@Table(name = "BC_WORK_TIME")
public class WorkTime {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "ID")
    private Long id;

    @Column(name = "DATE", nullable = false)
    @Temporal(TemporalType.DATE)
    private Date date;

    @Column(name = "TITLE", nullable = false)
    private String title;

    @Column(name = "DESCRIPTION", nullable = true)
    private String description;

    @Column(name = "TIME_WORKED_IN_SECONDS", nullable = false)
    private Integer timeWorkedSeconds = 0;

    @ManyToMany
    @JoinTable(name="BC_WORK_TIME_TAG",
            joinColumns=
            @JoinColumn(name="WORK_TIME_ID", referencedColumnName="ID"),
            inverseJoinColumns=
            @JoinColumn(name="TAG_ID", referencedColumnName="ID")
    )
    private Set<Tag> tags = new TreeSet<>();

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Integer getTimeWorkedSeconds() {
        return timeWorkedSeconds;
    }

    public void setTimeWorkedSeconds(Integer timeWorkedSeconds) {
        this.timeWorkedSeconds = timeWorkedSeconds;
    }

    public Set<Tag> getTags() {
        return tags;
    }

    public void removeTag(Tag tag) {
        this.tags.remove(tag);
        tag.getWorkTimes().remove(this);
    }

    public void addTag(Tag tag) {
        this.tags.add(tag);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        WorkTime workTime = (WorkTime) o;
        return Objects.equals(id, workTime.id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id);
    }
}