package ch.bytecrowd.worktimetracker.service;

import ch.bytecrowd.worktimetracker.model.WorkTime;
import ch.bytecrowd.worktimetracker.repository.WorkTimeRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;

@Service
public class WorkTimeService {

    private final WorkTimeRepository repository;

    @Autowired
    public WorkTimeService(WorkTimeRepository repository) {
        this.repository = repository;
    }

    public WorkTime createRecord(WorkTime record) {
        record.setId(null);
        return this.repository.save(record);
    }

    public List<WorkTime> fetchRecordsByDateOrdered(Date date) {
        return this.repository.findAllByDateOrderedByDateTitle(date);
    }
    public List<WorkTime> fetchRecordsByDateFromToInclOrdered(Date dateFrom, Date dateTo) {
        return this.repository.findAllByDateFromToInclOrderedByDateTitle(dateFrom, dateTo);
    }

    public WorkTime updateRecord(WorkTime record) {
        if (record.getId() == null)
            throw new IllegalArgumentException("WorkTime with null id can not be updated");
        return this.repository.save(record);
    }

    public void deleteRecordById(Long id) {
        final WorkTime workTime = repository.findById(id).orElseThrow(() -> new IllegalArgumentException("WorkTime with null id: " + id + " is already deleted"));
        workTime.getTags().forEach(tag -> workTime.removeTag(tag));
        this.repository.delete(workTime);
    }
}