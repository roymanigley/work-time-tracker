package ch.bytecrowd.worktimetracker.service;

import ch.bytecrowd.worktimetracker.model.Tag;
import ch.bytecrowd.worktimetracker.model.WorkTime;
import ch.bytecrowd.worktimetracker.repository.TagRepository;
import ch.bytecrowd.worktimetracker.repository.WorkTimeRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;

@Service
public class TagService {

    private final WorkTimeRepository workTimeRepository;
    private final TagRepository repository;

    @Autowired
    public TagService(WorkTimeRepository workTimeRepository, TagRepository repository) {
        this.workTimeRepository = workTimeRepository;
        this.repository = repository;
    }

    public List<Tag> fetchRecordsAll() {
        return this.repository.findAll(Sort.by(Sort.Order.asc("label")));
    }


    public Tag addTag(Long idWorkTime, Tag tag) {
        final WorkTime workTime = workTimeRepository.findById(idWorkTime)
                .orElseThrow(() -> new IllegalArgumentException("Tag with id: " + idWorkTime + " not found"));

        final List<Tag> allByLabelLike = repository.findAllByLabelLike(tag.getLabel());

        if (!allByLabelLike.isEmpty())
            tag = allByLabelLike.get(0);

        workTime.addTag(tag);
        tag = repository.save(tag);
        workTimeRepository.save(workTime);
        return tag;
    }

    public void removeTag(Long idWorkTime, Long idTag) {
        final WorkTime workTime = workTimeRepository.findById(idWorkTime)
                .orElseThrow(() -> new IllegalArgumentException("Tag with id: " + idWorkTime + " not found"));
        final Tag tag = repository.findById(idTag)
                .orElseThrow(() -> new IllegalArgumentException("Tag with id: " + idTag + " not found"));

        workTime.removeTag(tag);
        repository.save(tag);
        workTimeRepository.save(workTime);
    }

    /**
     * @info deletes only if the record has no relation to a WorkTime
     * @param id
     */
    public void deleteRecordById(Long id) {
        try {
            this.repository.deleteById(id);
        } catch (DataIntegrityViolationException e) {
            throw new IllegalArgumentException("Tag not deleted, is in relation with WorkTime", e);
        }
    }
}