package ch.bytecrowd.worktimetracker.rest;

import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpSession;
import java.util.LinkedHashMap;
import java.util.Map;

@RestController
@RequestMapping("/api/User")
public class AuthController {

    @RequestMapping(method = RequestMethod.GET, path = "/")
    public Map<String, String> userInfo(HttpSession session) {
        final Map<String, String> userInfo = new LinkedHashMap<>();
        userInfo.put("USERNAME", SecurityContextHolder.getContext().getAuthentication().getName());
        userInfo.put("TOKEN", session.getId());
        return userInfo;
    }
}
