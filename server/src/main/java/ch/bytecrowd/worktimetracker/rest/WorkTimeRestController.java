package ch.bytecrowd.worktimetracker.rest;

import ch.bytecrowd.worktimetracker.model.WorkTime;
import ch.bytecrowd.worktimetracker.service.WorkTimeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.web.bind.annotation.*;

import java.util.Date;
import java.util.List;

@RestController
@RequestMapping("/api/WorkTime")
@CrossOrigin
public class WorkTimeRestController {

    private final WorkTimeService service;

    @Autowired
    public WorkTimeRestController(WorkTimeService repository) {
        this.service = repository;
    }

    @RequestMapping(method = RequestMethod.POST, path = "/")
    public WorkTime createRecord(@RequestBody WorkTime record) {
        return service.createRecord(record);
    }

    @RequestMapping(method = RequestMethod.GET, path = "/{date}")
    public List<WorkTime> fetchRecordsByDate(@PathVariable @DateTimeFormat(pattern = "yyyy-MM-dd") Date date) {
        return service.fetchRecordsByDateOrdered(date);
    }

    @RequestMapping(method = RequestMethod.GET, path = "/{dateFrom}/{dateTo}")
    public List<WorkTime> fetchRecordsByDateFromToIncl(@PathVariable @DateTimeFormat(pattern = "yyyy-MM-dd") Date dateFrom, @PathVariable @DateTimeFormat(pattern = "yyyy-MM-dd") Date dateTo) {
        return service.fetchRecordsByDateFromToInclOrdered(dateFrom, dateTo);
    }

    @RequestMapping(method = RequestMethod.PUT, path = "/{id}")
    public WorkTime updateRecord(@PathVariable Long id, @RequestBody WorkTime record) {
        record.setId(id);
        return service.updateRecord(record);
    }

    @RequestMapping(method = RequestMethod.DELETE, path = "/{id}")
    public void deleteRecord(@PathVariable Long id) {
        service.deleteRecordById(id);
    }

}