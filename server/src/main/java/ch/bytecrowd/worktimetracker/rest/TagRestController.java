package ch.bytecrowd.worktimetracker.rest;

import ch.bytecrowd.worktimetracker.model.Tag;
import ch.bytecrowd.worktimetracker.model.WorkTime;
import ch.bytecrowd.worktimetracker.service.TagService;
import ch.bytecrowd.worktimetracker.service.WorkTimeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.web.bind.annotation.*;

import java.util.Date;
import java.util.List;

@RestController
@RequestMapping("/api")
@CrossOrigin
public class TagRestController {

    private final TagService service;

    @Autowired
    public TagRestController(TagService repository) {
        this.service = repository;
    }

    @RequestMapping(method = RequestMethod.GET, path = "/Tag/")
    public List<Tag> fetchAllRecords() {
        return service.fetchRecordsAll();
    }

    @RequestMapping(method = RequestMethod.POST, path = "/WorkTime/{idWorkTime}/Tag")
    public Tag addTag(@PathVariable Long idWorkTime, @RequestBody Tag tag) {
        return service.addTag(idWorkTime, tag);
    }

    @RequestMapping(method = RequestMethod.DELETE, path = "/WorkTime/{idWorkTime}/Tag/{idTag}")
    public void removeTag(@PathVariable Long idWorkTime, @PathVariable Long idTag) {
        service.removeTag(idWorkTime, idTag);
    }

    @RequestMapping(method = RequestMethod.DELETE, path = "/Tag/{id}")
    public void deleteRecord(@PathVariable Long id) {
        service.deleteRecordById(id);
    }
}