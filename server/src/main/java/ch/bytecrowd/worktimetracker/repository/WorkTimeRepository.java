package ch.bytecrowd.worktimetracker.repository;

import ch.bytecrowd.worktimetracker.model.WorkTime;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.Date;
import java.util.List;

public interface WorkTimeRepository extends JpaRepository<WorkTime, Long> {

    @Query("from WorkTime w where w.date = :date order by w.date desc, w.title")
    public List<WorkTime> findAllByDateOrderedByDateTitle(@Param("date") Date date);

    @Query("from WorkTime w where w.date >= :dateFrom and w.date <= :dateTo order by w.date desc, w.title")
    public List<WorkTime> findAllByDateFromToInclOrderedByDateTitle(@Param("dateFrom") Date dateFrom, @Param("dateTo") Date dateTo);
}