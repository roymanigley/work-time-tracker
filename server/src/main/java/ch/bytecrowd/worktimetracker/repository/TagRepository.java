package ch.bytecrowd.worktimetracker.repository;

import ch.bytecrowd.worktimetracker.model.Tag;
import ch.bytecrowd.worktimetracker.model.WorkTime;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface TagRepository extends JpaRepository<Tag, Long> {

    public List<Tag> findAllByLabelLike(String label);
}